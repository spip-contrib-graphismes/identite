# Polatouche

Auteur : M. Charles D'Orbigny

Source : (1849) Dictionnaire Universel d'Histoire Naturelle. Atlas., Paris: MM. Renard, Martinet et Cie. 

Licence : domaine public

Sources :

1. https://archive.org/details/dictionnaireuniv11849orbi/page/n65/mode/2up
2. https://commons.wikimedia.org/wiki/File:FMIB_46817_Polatouche_d%27Amerique.jpeg
3. http://ctgpublishing.com/albatross-illustration-by-edouard-travies/flying-squirrel-illustration-by-edouard-travies/


## Notes

Plusieurs versions sont disponibles en ligne, les images de ce dépôt sont issues de la 3), qui est de meilleure qualité.

La planche originale étant légèrement floue, les images détourées ont été passées à la moulinette pour récupérer les détails perdus tout en conservant la résolution (algorithme « Smart Enhance » sur https://letsenhance.io)