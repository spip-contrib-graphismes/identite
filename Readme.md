# Identité graphique SPIP

Ce dépôt contient tous les éléments relatifs à l’identité graphique de SPIP :

* design_system : logo, police et futur design system.
* illustrations : images diverses, dont celles utilisées pour les annonces, et la gravure de polatouche.
* archives : anciens logos et cie.