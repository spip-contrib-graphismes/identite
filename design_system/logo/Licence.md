# Licence

Cette œuvre est mise à disposition sous licence Attribution -  Partage dans les Mêmes Conditions 2.0 France. 
Pour voir une copie de cette licence, visitez http://creativecommons.org/licenses/by-sa/2.0/fr/ ou écrivez à Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

Le logo, la favicon et leurs déclinaisons ont été créés en janvier 2017 par Jordan Zucchiatti (jordanzucchiatti@gmail.com)